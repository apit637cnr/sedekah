-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 09, 2021 at 10:42 PM
-- Server version: 10.3.28-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u9473027_sedekah`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `jumlah_donasi` varchar(25) NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  `no_wa` varchar(25) NOT NULL,
  `norek` varchar(25) NOT NULL,
  `nama_bank` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `nama`, `jumlah_donasi`, `keterangan`, `no_wa`, `norek`, `nama_bank`) VALUES
(9, 'fahmi', '100000', 'buat yatim', '085692641769', '0523 0100 0259 302', 'bri');

-- --------------------------------------------------------

--
-- Table structure for table `bukti_transfer`
--

CREATE TABLE `bukti_transfer` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `donasi` varchar(100) NOT NULL,
  `resi` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `donasi`
--

CREATE TABLE `donasi` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `no_telphon` varchar(25) NOT NULL,
  `jumlah_donasi` varchar(25) NOT NULL,
  `melalui_bank` varchar(25) NOT NULL,
  `keterangan` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `galang_dana`
--

CREATE TABLE `galang_dana` (
  `id` int(11) NOT NULL,
  `target` varchar(255) NOT NULL,
  `nama` text NOT NULL,
  `alamat` text NOT NULL,
  `cerita` text NOT NULL,
  `foto_kampanye` varchar(25) NOT NULL,
  `waktu` int(11) DEFAULT NULL,
  `judul` varchar(25) NOT NULL,
  `tanggal_awal` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `galang_dana`
--

INSERT INTO `galang_dana` (`id`, `target`, `nama`, `alamat`, `cerita`, `foto_kampanye`, `waktu`, `judul`, `tanggal_awal`, `tanggal_akhir`) VALUES
(1, '20.000.000', 'Bantu Yatim Untuk Sembuh Dari Penyakit Thalasemia', 'Jl. Masjid Al - Mujahidin RT. 02 RW. 06 Kel. Meruyung Kec. Limo Kota Depok 16515', '\r\nRIZAL, YATIM PIATU PENDERITA THALASEMIA\r\nDARI 3 MINGGU SEKALI TRANSFUSI HINGGA TERLANTAR DIRUMAH SAKIT\r\nMuhammad Rizal, lahir pada tanggal 26 Januari 2005 . Semenjak kecil ia sudah ditinggal ayah dan ibunya, ia dirawat oleh kakek serta neneknya , 3 Minggu sekali Rizal harus Transfusi darah di Rumah Sakit PMI Bogor sejak duduk dibangku kelas 1 Sekolah Dasar sekarang usia Rizal 15 Tahun, Jika Rizal telat mendapatkan transfusi Darah kondisi rizal drop, bermandikan keringat, dan tidak bisa apa apa. Saat pertama kali penyakit Rizal terdeteksi ia sedang berada di sekolah dan tidak bisa berbuat apa apa, atas saran guru rizal untuk periksa darah akhirnya baru terdeteksi jika saat itu rizal sudah menderita Thalasemia. Untuk diketahui Thalasemia adalah ketidakmampuan memproduksi sel darah merah dan hemoglobin.\r\n sel-sel darah merah juga memiliki peran penting mempertahankan kehidupan termasuk dalam pengangkutan oksigen. Oksigen juga bertanggung jawab dalam berbagai fungsi, utamanya sintesis sel. Oksigen juga berperan penting dalam sistem metabolisme tubuh. Untuk mengangkut oksigen ke seluruh bagian tubuh, oksigen mengikatkan diri ke hemoglobin yang menghasilkan warna merah pada sel. Hal ini memungkinkan sel untuk mengangkut oksigen dari paru-paru menuju ke jaringan tubuh lainnya.\r\nKetika seseorang memiliki thalasemia, itu artinya salah satu dari komponen terpenting dalam struktur hemoglobin telah hilang atau rusak (diubah). gangguan ini dapat menciptakan kondisi yang nihil sampai pada gejala yang mengancam keberlangsungan hidup penderita.\r\n Di saat pandemic ini Nenek Rizal bersyukur tidak mengganggu jadwal transfuse Rizal . Rizal terkadang mengalami kesulitan stok darah karena tidak tersedianya kantong darah di Rumah Sakit, di kondisi seperti itu sang nenek berusaha semampunya untuk mempertahankan rizal sembari mencari pendonor yang cocok . untuk saat ini rizal belum mempunyai pendonor tetap. 	Untuk sekali kunjungan kerumah sakit minimal nenek rizal harus merogoh koceknya sebanyak 300.000 . Nenek ( Nama nenek rizal ) bersyukur karena telah dibantu oleh Yayasan Alkirom Amanah sehingga perawatan Rizal terus berjalan lancar. Satu kali transfuse Rizal menghabiskan  1-2 kantong darah . saat awal ditinggal ayah sempat shock dan suka melamun tidak lama ayahnya meninggal ibunya juga dipanggil Allah SWT. Nenek Boni sangat bersyukur telah dibantu oleh Yayasan alkirom amanah . Pada saat malam sebelum pilkada kota Depok Rizal mengalami patah tulang kanan karena bercanda dengan teman temannya dan sempat dirawat di rumah sakit Hermina, Pada saat di Rumah sakit inilah Rizal mengalami kisah yang cukup mengharukan . dikarenakan ia memiliki penyakit thalesemia Dokter yang menanganinya tidak berani mengambil tindakan yang dinilai cukup beresiko. Akhirnya atas keputusan dokter dan keluarga Rizal , diputuskan untuk mengobati lewat ahli patah tulang dengan kondisi tulang tangan yang patah itu rizal harus menggunakan Penyangga Lengan yang dimana harus membayar sebesar 80.000 sedangkan disaat itu nenek bony hanya mengantongi uang 30.000 untuk ongkos pulang, suster rumah sakit berinisiatif untuk menahan KIS milik Rizal untuk dijadikan jaminan dan uang 30.000 itu, masalah tidak usai sampai disitu kini rizal dan neneknya pun bingung ingin pulang bagaimana, tantenya yang sedang bertugas di TPS tidak bisa dihubungi dari jam 09.00 pagi hingga 17.00 Rizal & neneknya memikirkan bagaimana caranya agar mereka bisa pulang, tentunya dengan kondisi tangan seperti itu dan rasa lapar yang mendera Rizal yang masih kecil meringkih kesakitan. Melihat seorang nenek yang sedang kebingungan akhirnya ada orang baik yang menghampiri, ia menanyakan kondisinya bagaimana . Setelah dijelaskan orang baik itu menuju resepsionis dan menebus utang rizal & neneknya serta membantunya pulang dengan memesan grabcar. Nenek rizal sangat berterima kasih kepada yang telah membantunya semoga semua kebaikannya diganti oleh Allah SWT dengan sebaik baiknya pengganti. \r\n\r\nSalurkan donasi Anda dengan cara:\r\n\r\n1. Klik \"DONASI SEKARANG\"\r\n2. Pilih Bank Transfer BRI dan BNI\r\n\r\nBantu share juga halaman ini agar lebih banyak doa dan bantuan yang terkumpul.', 'IMG_7155.JPG', 90, 'Ibu Halimah', '2021-03-08', '2021-06-06'),
(2, '50.000.000', 'Sembuhkan Yatim Penderita Syndrom Rubella', 'Jl. Bojong Kel. Meruyung Kec. Limo Kota Depok', 'Sejak 31 Januari 2021 ( Farzhan althafariz Setiawan ) ditinggal ayahnya karena penyakit yg di derita pada usia 49 Tahun, dan meninggalkan dua orang anak farzha & kakanya yang berusia 9 tahun, sehari hari sang ibu ( Novi 31, Tahun ) hanya bisa focus menjaga ( farzhan ) karena penyakit ( nama penyakit ) , akibat yang dideritanya fadlan seringkali muntah secara tiba tiba . Untuk memenuhi kehidupan sehari hari fadlan & keluarganya bersyukur banyak saudara & tetangga yang mendukung. Penyakit yang diderita Farzhan membuat ia hanya bisa mengkonsumsi susu untuk memenuhi kebutuhan proteinnya, kondisi seperti ini dialamai farzhan semenjak lahir menurut diagnose dokter yang memeriksanya ia mengidap sindrom rubella yang menyerang mata dan mengakibatkan pembengkakan ginjal oleh karena itu farzhan harus control rutin ke dokter tetapi untuk matanya sudah tidak bisa diambil tindakan apa apa , farzhan hanya bisa melihat sedikit cahaya dan berbicara tidak jelas . saat ini farzhan sedang menjalani terapi namun dampak dari Covid-19 membuat farzhan terpaksa memberhentikan sementara terapinya dan menjalani terapi dirumah dibnatu oleh sang ibu, farzhan harus terus meminum obat herbal & antibiotic . Keluarga farzhan mengenal Yayasan Alkirom Amanah dari tetangganya dan merasa terbantu karena kebutuhan sehari hari farzhan cukup banyak seperti Susu, Tisu Basah, Tisu Kering, dan Pampers, dalam 3 hari farzhan membutuhkan satu kaleng susu serta menghabiskan 4 bal pampers dalam sebulan karena mengingat kondisi fisiknya farzhan tidak seperti anak lainnya.  Ibu novi berharap semoga kedepannya kanza kaka farzhan dapat menempuh Pendidikan yang tinggi dan terus menjaga adiknya ', 'fazhran.jpg', 90, 'Novi', '2021-03-08', '2021-06-06'),
(3, '25.000.000', 'Yatim Penderita Thalasemia', 'Jl. Raya Bogor ', 'Perkenalkan adik kita yang bernama agung ia mengidap \r\npenyakit thalasemia yang mengharuskannya tranfusi darah \r\n10 hari sekali,Kami ingin meminta doa kepada para sahabat\r\nsemoga adik kita di berikan semangat untuk\r\nkembali sehat dan bisa beraktivitas lagi seperti biasa \r\ndan jika sahabat berkenan ingin meringankan dan membantu \r\nadik kita dengan senang hatiakan menerimanya', 'agung.jpg', 90, 'Agung', '2021-03-08', '2021-06-06'),
(4, '50.000.000', 'Bantu adik adik kami meraih cita citanya', 'Jl Gg H.Midan RT01/05', 'Pelajar Mengajar adalah sebuah organisasi yang bergerak di bidang pendidikan anak-anak khususnya di daerah krukut. Organisasi ini berdiri sejak tahun 2019, memulai dengan memberikan pelajaran pada anak-anak di daerah Gunung Balong Lebak Bulus, kini organisasi ini berpindah ke Jalan Midan, Krukut-Depok. \r\nKegiatan Kami\r\n1. Belajar Ilmu Pengetahuan Umum dan Komputer\r\n2. Pendalaman Materi Sekolah\r\n3. Kegiatan Hari Besar Keagamaan \r\n4.Kegiatan Hari Besar Nasional\r\nTarget Kami Dalam Mengajar Adalah Meningkatkan ilmu pengetahuan umum, komputer, dan budaya..\r\nDan Donasi ini akan kami salurkan kepada anak didik di PELAJAR MENGAJAR, tujuan nya supaya mempermudah dalam hal belajar mulai dari bentuk materi ataupun konsumsi anak anak.', 'IMG_20201122_160713.jpg', 60, 'Pelajar Mengajar', '2021-03-08', '2021-05-07'),
(5, '100.000.000', 'Tebar Senyum Yatim Pelosok', 'jalan raya meruyung', 'Sahabat Kemanusiaan,\r\nMenyantuni anak yatim adalah akhlak mulia dan moralitas kemanusiaan yang tinggi. Siapakah anak yatim? Yaitu seorang anak yang ditinggal mati oleh ayahnya dan ia belum mencapai usia akil baligh atau dewasa. Ia membutuhkan pertolongan dan kasih sayang dari kita orang yang ada disekitarnya. Karena ia tidak mungkin mendapatkan kasih sayang ayahnya yang telah tiada. Seorang yang penyayang kepada anak-anak yatim dan menyantuni mereka adalah seorang yang berbudi dan berakhlak mulia. Itulah pesan Nabi kepada Saib, sahabatnya:\r\n\r\nâ€œWahai Saib, perhatikanlah akhlak yang biasa kamu lakukan ketika kamu masih dalam kejahiliyahan, laksanakan pula ia dalam masa keislaman. Jamulah tamu, muliakanlah anak yatim, dan berbuat baiklah kepada tetangga.â€ [HR. Ahmad dan Abu Dawud]', 'yatim pelosok.jpg', 90, 'Khalid', '2021-03-08', '2021-06-06');

-- --------------------------------------------------------

--
-- Table structure for table `konfirmasi_pembayaran`
--

CREATE TABLE `konfirmasi_pembayaran` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `rupiah` varchar(25) NOT NULL,
  `img` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `konfirmasi_pembayaran`
--

INSERT INTO `konfirmasi_pembayaran` (`id`, `nama`, `rupiah`, `img`) VALUES
(5, 'fahmi', '100,000', 'agung.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(25) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `level`) VALUES
(1, 'nasuha', 'muhammadnassuha7@gmail.com', 'nasuha123', ''),
(2, 'rete', 'tes@gmail.com', '123', ''),
(3, 'Indar Kholifatul Sholikha', 'indarsholikhah24@gmail.com', '230200', ''),
(6, 'Fahmi', 'fahmiiabd@gmail.com', '90512655', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `id` int(11) NOT NULL,
  `email` varchar(25) NOT NULL,
  `username` varchar(25) NOT NULL,
  `nama` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`id`, `email`, `username`, `nama`) VALUES
(1, 'muhammadnassuha7@gmail.co', 'muhammad', 'muhammad nassuha'),
(2, 'muhammadnassuha7@gmail.co', 'muhammad', 'muhammad nassuha'),
(3, 'muhammadnassuha7@gmail.co', 'muhammad', 'muhammad nassuha'),
(4, 'muhammadnassuha7@gmail.co', 'muhammad', 'muhammad nassuha'),
(5, 'muhammadnassuha7@gmail.co', 'muhammad', 'muhammad nassuha'),
(6, 'muhammadnassuha7@gmail.co', 'muhammad', 'muhammad nassuha'),
(7, 'muhammadnassuha7@gmail.co', 'muhammad', 'muhammad nassuha'),
(8, 'muhammadnassuha7@gmail.co', 'muhammad', 'muhammad nassuha'),
(9, 'muhammadnassuha7@gmail.co', 'muhammad', 'muhammad nassuha'),
(10, 'muhammadnassuha7@gmail.co', 'muhammad', 'muhammad nassuha'),
(11, 'muhammadnassuha7@gmail.co', 'muhammad', 'muhammad nassuha'),
(12, 'muhammadnassuha7@gmail.co', 'muhammad', 'muhammad nassuha');

-- --------------------------------------------------------

--
-- Table structure for table `verifikasi_table`
--

CREATE TABLE `verifikasi_table` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `no_ktp` varchar(25) NOT NULL,
  `bukti_ktp` varchar(25) NOT NULL,
  `tanggal` datetime NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donasi`
--
ALTER TABLE `donasi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `galang_dana`
--
ALTER TABLE `galang_dana`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `konfirmasi_pembayaran`
--
ALTER TABLE `konfirmasi_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verifikasi_table`
--
ALTER TABLE `verifikasi_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `donasi`
--
ALTER TABLE `donasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `galang_dana`
--
ALTER TABLE `galang_dana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `konfirmasi_pembayaran`
--
ALTER TABLE `konfirmasi_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `verifikasi_table`
--
ALTER TABLE `verifikasi_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
