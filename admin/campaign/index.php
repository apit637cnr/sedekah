<h1>Campaign</h1>


	<div class="row">
	<!-- customar project  start -->
	<div class="col-xl-12">
		<div class="card">
			<div class="card-body">
				<div class="row align-items-center m-l-0">
					<div class="col-sm-6"></div>
					<div class="col-sm-6 text-right">
						<button class="btn btn-success btn-sm btn-round has-ripple" data-toggle="modal" data-target="#modal-report"><i class="feather icon-plus"></i> Add Pharmacist</button>
					</div>
				</div>
				<div class="table-responsive">
					<div id="report-table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
						<div class="row">
							<div class="col-sm-12 col-md-6">
								<div class="dataTables_length" id="report-table_length">
									<label>Show
									<select name="report-table_length" aria-controls="report-table" class="custom-select custom-select-sm form-control form-control-sm">
										<option value="10">3</option>
										<option value="25">25</option>
										<option value="50">50</option>
										<option value="100">100</option>
									</select>
									 entries</label>
								</div>
							</div>
							<div class="col-sm-12 col-md-6">
								<div id="report-table_filter" class="dataTables_filter">
									<label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="report-table"></label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<table id="report-table" class="table table-bordered table-striped mb-0 dataTable no-footer" role="grid" aria-describedby="report-table_info">
								<thead>
								<tr role="row">
									<th class="sorting" tabindex="0" aria-controls="report-table" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 104px;">No</th>
									<th class="sorting" tabindex="0" aria-controls="report-table" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 104px;">Target</th>
									<th class="sorting" tabindex="0" aria-controls="report-table" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 104px;">Nama</th>
									<th class="sorting" tabindex="0" aria-controls="report-table" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending" style="width: 210px;">Alamat</th>
									
									<th class="sorting" tabindex="0" aria-controls="report-table" rowspan="1" colspan="1" aria-label="Phone: activate to sort column ascending" style="width: 134px;">Deskripsi</th>
									<th class="sorting_asc" tabindex="0" aria-controls="report-table" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Image: activate to sort column descending" style="width: 65px;">Image</th>
									<th class="sorting" tabindex="0" aria-controls="report-table" rowspan="1" colspan="1" aria-label="Phone: activate to sort column ascending" style="width: 134px;">Waktu</th>
									<th class="sorting" tabindex="0" aria-controls="report-table" rowspan="1" colspan="1" aria-label="Options: activate to sort column ascending" style="width: 172px;">Judul</th>
									<th class="sorting" tabindex="0" aria-controls="report-table" rowspan="1" colspan="1" aria-label="Options: activate to sort column ascending" style="width: 172px;">Tanggal Awal</th>
									<th class="sorting" tabindex="0" aria-controls="report-table" rowspan="1" colspan="1" aria-label="Options: activate to sort column ascending" style="width: 172px;">Tanggal Akhir</th>
									<th class="sorting" tabindex="0" aria-controls="report-table" rowspan="1" colspan="1" aria-label="Options: activate to sort column ascending" style="width: 172px;">Action</th>
								</tr>
								</thead>
								<tbody>
								<tr role="row" class="odd">
									
								<?php
								include "../config/config.php" ;
								$result = mysqli_query($query, "SELECT * FROM galang_dana");
								$no = 1;
								while ($hasil = mysqli_fetch_array($result)) {

									?>
									<tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $hasil['target']; ?></td>
										<td><?php echo $hasil['nama']; ?></td>
										<td><?php echo $hasil['alamat']; ?></td>
										<td><?php echo $hasil['cerita']; ?></td>
										<td><?php echo "<img src='img/$hasil[foto_kampanye]' 'img-fluid img-radius wid-40' width='70' height='60' />";?></td>
										<td><?php echo $hasil['waktu']; ?></td>
										<td><?php echo $hasil['judul']; ?></td>
										<td><?php echo $hasil['tanggal_awal']; ?></td>
										<td><?php echo $hasil['tanggal_akhir']; ?></td>
									<!-- <td class="sorting_1">
										<img src="assets/images/user/avatar-1.jpg" class="img-fluid img-radius wid-40" alt="">
									</td>
 -->									<td>
										<a href="#!" class="btn btn-info btn-sm"><i class="feather icon-edit"></i>&nbsp;Edit </a>
										<a href="index.php?halaman=deleteCampaign&id=<?php echo $hasil['id']; ?>" class="btn btn-danger btn-sm"><i class="feather icon-trash-2"></i>&nbsp;Delete </a>
									</td>
								</tr>
							<?php } ?>
								</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 col-md-5">
								<div class="dataTables_info" id="report-table_info" role="status" aria-live="polite">Showing 1 to 4 of 4 entries</div>
							</div>
							<div class="col-sm-12 col-md-7">
								<div class="dataTables_paginate paging_simple_numbers" id="report-table_paginate">
									<ul class="pagination">
										<li class="paginate_button page-item previous disabled" id="report-table_previous">
											<a href="#" aria-controls="report-table" data-dt-idx="0" tabindex="0" class="page-link">Previous</a>
										</li>
										<li class="paginate_button page-item active">
											<a href="#" aria-controls="report-table" data-dt-idx="1" tabindex="0" class="page-link">1</a>
										</li>
										<li class="paginate_button page-item next disabled" id="report-table_next">
											<a href="#" aria-controls="report-table" data-dt-idx="2" tabindex="0" class="page-link">Next</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- customar project  end --></div>