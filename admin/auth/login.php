

<!DOCTYPE html>
<html lang="en">

<head>

	<title>Ablepro v8.0 bootstrap admin template by Phoenixcoded</title>
	<!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 11]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="" />
	<meta name="keywords" content="">
	<meta name="author" content="Phoenixcoded" />
	<!-- Favicon icon -->
	<link rel="icon" href="../assets/images/favicon.ico" type="image/x-icon">

	<!-- vendor css -->
	<link rel="stylesheet" href="../assets/css/style.css">
	
	


</head>

<!-- [ signin-img-tabs ] start -->
<div class="blur-bg-images"></div>
<div class="auth-wrapper">
	<div class="auth-content">
		<div class="card text-center">
			<div class="card-body">
				<form method="post" action="prosess_login.php">
				<h3 class="mb-3">Selamat Datang Di <span class="text-c-blue">Data Admin Dypa</span></h3>
				<p>Masukan Email Dan Password Dengan Benar!</p>
				<div class="toggle-block">
					<ol class="position-relative carousel-indicators justify-content-center">
						<li class="toggle-btn"></li>
						<li class="active"></li>
					</ol>
					<div class="form-group mb-3">
						<label class="floating-label" for="Email">Email address</label>
						<input type="email" name="email" class="form-control" id="Email">
					</div>
					<div class="form-group mb-3">
						<label class="floating-label" for="Password">Password</label>
						<input type="password" name="password" class="form-control" id="Password">
					</div>
					<div class="custom-control custom-checkbox text-left mb-4 mt-2">
						<input type="checkbox" class="custom-control-input" id="customCheck1">
						<label class="custom-control-label" for="customCheck1">Save credentials</label>
					</div>
					<button class="btn btn-primary mb-4" name="login">Signin</button>
					<!-- <button class="btn btn-outline-primary mb-4 toggle-btn">Create Profile</button> -->
					
				</div>
				</form>
			
				<!-- <div class="toggle-block collapse">
					<ol class="position-relative carousel-indicators justify-content-center">
						<li class="active"></li>
						<li class="toggle-btn"></li>
					</ol>
					<div class="form-group mb-3">
						<label class="floating-label" for="Username">Username</label>
						<input type="text" class="form-control" id="Username">
					</div>
					<div class="form-group mb-3">
						<label class="floating-label" for="Email">Email address</label>
						<input type="email" class="form-control" id="Email">
					</div>
					<div class="form-group mb-3">
						<label class="floating-label" for="Password">Password</label>
						<input type="password" class="form-control" id="Password">
					</div>
					<div class="custom-control custom-checkbox text-left mb-4 mt-2">
						<input type="checkbox" class="custom-control-input" id="customCheck1">
						<label class="custom-control-label" for="customCheck1">Send me the <a href="#!"> Newsletter</a> weekly.</label>
					</div>
					<button class="btn btn-primary mb-4">Signup</button>
					<button class="btn btn-outline-primary mb-4 toggle-btn">Existing user</button>
				</div> -->
			</div>
		</div>
	</div>
</div>
<!-- [ signin-img-tabs ] end -->

<!-- Required Js -->
<script src="../assets/js/vendor-all.min.js"></script>
<script src="../assets/js/plugins/bootstrap.min.js"></script>
<script src="../assets/js/ripple.js"></script>
<script src="../assets/js/pcoded.min.js"></script>
<script>
	$('.toggle-btn').on('click', function() {
		$('.toggle-block').toggle();
	})
</script>




</body>

</html>
